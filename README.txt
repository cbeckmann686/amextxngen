## BUILDING

Project can be compiled using the following command:

    mvn clean compile assembly:single

Jar file will be produced in "target" directory.

## RUNNING

Execute jar using either Java 8 or 11 runtime, as follows:

java -jar AxemTxnGen-0.1-SNAPSHOT-jar-with-dependencies.jar -o outputDir [-np]