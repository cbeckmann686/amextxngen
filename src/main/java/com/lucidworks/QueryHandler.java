package com.lucidworks;

import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class QueryHandler
{
    private URL queryEndpoint;
    private BlockingQueue<IngestedRecord> records;
    private ExecutorService executor;
    private List<Runnable> workers;
    private Semaphore availableRecord;
    protected Logger log;
    private boolean stopping;
    private Stopwatch swQuery;
    private int waitSeconds;
    private int numCustomersGenerated;
    private int numRecordsGenerated;
    private String authHeader;
    private int backoff;

    public QueryHandler(String endpoint, int numThreads, int waitSeconds, int backoff, String authHeader)
    {
        log = LogManager.getLogger(QueryHandler.class);
        executor = Executors.newFixedThreadPool(numThreads);
        workers = new ArrayList<>();
        availableRecord = new Semaphore(numThreads, true);
        records = new LinkedBlockingQueue<>();
        swQuery = Stopwatch.createUnstarted();
        stopping = false;
        this.waitSeconds = waitSeconds;
        numCustomersGenerated = 0;
        numRecordsGenerated = 0;
        this.authHeader = authHeader;
        this.backoff = backoff;

        try
        {
            queryEndpoint = new URL(endpoint);
            log.info("Started QueryHandler for " + endpoint + " with " + numThreads + " threads.");

            if (!verifyEndpoint())
            {
                log.error("Unable to verify query endpoint. Application will now exit.");
                System.exit(1);
            }

            swQuery.start();

            for (int i = 0; i < numThreads; i++)
            {
                Runnable worker = new QueryWorker("Query " + i, endpoint, this, waitSeconds, backoff, authHeader);
                workers.add(worker);
                executor.execute(worker);
            }

        }
        catch (MalformedURLException me)
        {
            log.error("Unable to parse query url: " + queryEndpoint + ". Error: " + me.getMessage());
            System.exit(1);
        }
    }

    public void startTimer()
    {
        swQuery.start();
    }

    // public synchronized void addRecord(String accountToken) // sync keyword can trigger deadlock - CB
    public void addRecord(IngestedRecord rec)
    {
        records.add(rec);
        availableRecord.release();
    }

    public synchronized IngestedRecord getQueryRec()
    {
        if (records.isEmpty() && stopping) return null;

        try
        {
            availableRecord.acquire();
            return records.take();
        }
        catch (InterruptedException ie)
        {
            log.error(ie.getMessage());
            System.exit(1);
        }

        //unreachable
        return null;
    }

    public void setJobFinished(int numCustomersGenerated, int numRecordsGenerated, String ingestStats)
    {
        stopping = true;
        this.numRecordsGenerated = numRecordsGenerated;
        this.numCustomersGenerated = numCustomersGenerated;

        while (true)
        {
            try
            {
                if (records.isEmpty()) break;
                else
                {
                    sleep(1000);
                }
            }
            catch (InterruptedException e)
            {
                //no-op
            }
        }

        log.info("Waiting for query threads to finish...");
        executor.shutdown();
        try
        {
            executor.awaitTermination(10, TimeUnit.SECONDS);
        }
        catch (InterruptedException ie)
        {
            log.warn("Error encountered during query worker shutdown: " + ie.getMessage());
        }

        swQuery.stop();

        // subtract wait time from overall query duration
        Duration queryTime = swQuery.elapsed();
        queryTime = queryTime.minus(Duration.ofSeconds(waitSeconds));

        int totalSuccess = 0;
        int totalFailure = 0;
        int totalTimes = 0;
        int totalRetries = 0;
        long totalTransactions = 0;
        long availability = 0;
        for (Runnable w : workers)
        {
            totalSuccess += ((QueryWorker)w).getSucceeded();
            totalFailure += ((QueryWorker)w).getFailed();
            totalTimes += ((QueryWorker)w).getAverageTime();
            totalRetries += ((QueryWorker)w).getNumRetries();
            totalTransactions += ((QueryWorker)w).getTotalTransactions();
            availability += ((QueryWorker)w).getTotalAvailability();
        }

        // TODO coercing this log output into a stringbuilder will make it easier to read
        if (numCustomersGenerated > 0 && numRecordsGenerated > 0)
        {
            log.info("\n############ RESULTS ############");
            log.info(ingestStats);
            log.info("Query workers terminated. Total successful customer records queried: " + totalSuccess + ". Total Transactions found: "
                    + totalTransactions + ". Total failures: " + totalFailure + ". Elapsed time: " + queryTime.getSeconds() + " seconds.");
            log.info("Average HTTP query time for single customer: " + totalTimes / workers.size() + " ms.");
            log.info("Number of query retries: " + totalRetries);

            if (totalRetries > ((double)numCustomersGenerated * 0.05))  // warn about retries if > 5% of queries required one
            {
                log.warn("High retries required. Consider a longer delay interval to prevent underrun.");
            }

            Double successRate = ((double)totalTransactions / (double)numRecordsGenerated) * 100;
            log.info("Number of customers/transactions generated: " + numCustomersGenerated + "/" + numRecordsGenerated);
            log.info("Number of customers/transactions queried: " + totalSuccess + "/" + totalTransactions + " (" + String.format("%.2f", successRate) + "%)");
            log.info("Query records per second: " + String.format("%.2f", ((double)totalTransactions / queryTime.getSeconds())));

            double availabilityAvg = (double)availability / (double)numCustomersGenerated;
            int precisionMs = backoff + (waitSeconds * 1000);

            if (backoff > 500 || waitSeconds > 10)
            {
                log.warn("WARNING: Backoff and/or delay interval may impact record availability averages!");
            }

            log.info("Average record availability time: " + String.format("%.2f", availabilityAvg) + "ms (Precision: +/- " + precisionMs + "ms.)");

            if ((numCustomersGenerated == totalSuccess) && (numRecordsGenerated == totalTransactions))
            {
                log.info("Ingested and queried records match.");
            }
            else
            {
                log.warn("Mismatch between ingested and queried records!");
            }
        }

        // This is terrible, but without a central point of control it is necessary
        System.exit(0);
    }

    private boolean verifyEndpoint()
    {
        //for now
        try {
            HttpURLConnection con = (HttpURLConnection) queryEndpoint.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", authHeader);
            con.setRequestProperty("q", "q=account_token:1");

            con.setConnectTimeout(10000);
            con.setReadTimeout(10000);
            con.setInstanceFollowRedirects(false);
            int status = con.getResponseCode();

            InputStream is = con.getInputStream();
            while (is.read() != -1) { }
            is.close();
            con.disconnect();

            if (status >= 200 && status <= 299)
            {
                log.info("Query endpoint verified.");
                return true;
            }
            else
            {
                log.error("Unexpected response from query endpoint: HTTP " + status);
                return false;
            }
        }
        catch (IOException ie)
        {
            log.error("Invalid query URL: " + ie.getMessage());
            return false;
        }
    }
}
