package com.lucidworks;

import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class QueryWorker implements Runnable
{
    protected Logger log;
    private String name;
    private QueryHandler parent;
    private Stopwatch sw;
    private BigInteger totalMS;
    private long totalRecords;
    private long transactionsFound;
    private int querySucceeded;
    private int queryFailed;
    private int numRetries;
    private int waitSeconds;
    private URL queryEndpoint;
    private String authHeader;
    private int backoff;
    private long availabilityTotal;
    private HttpURLConnection con;

    public QueryWorker(String name, String queryEndpoint, QueryHandler parent, int waitSeconds, int backoff, String authHeader) throws MalformedURLException
    {
        log = LogManager.getLogger(QueryWorker.class);
        this.name = name;
        this.parent = parent;
        this.queryEndpoint = new URL(queryEndpoint);
        this.waitSeconds = waitSeconds;
        this.authHeader = authHeader;
        sw = Stopwatch.createUnstarted();
        totalMS = BigInteger.valueOf(0);
        totalRecords = 0;
        querySucceeded = 0;
        queryFailed = 0;
        transactionsFound = 0;
        numRetries = 0;
        availabilityTotal = 0;
        this.backoff = backoff;
    }

    // I had 99 problems before I used threads, now I ha107vePr oblems
    public void run()
    {
        log.info("Started query thread " + name + ".");

        try
        {
            if (waitSeconds > 0)
            {
                log.info("Query thread " + name + " sleeping for " + waitSeconds + " seconds.");
                sleep(waitSeconds * 1000);
                log.info("Query worker waking up.");
            } else
            {
                sleep(1000);  // just a touch of breathing room - only delays first round
            }
        }
        catch (InterruptedException ie)
        {
            log.error("Query worker " + name + " unexpectedly interrupted!");
            return;
        }

        while (true)
        {
            IngestedRecord ingestRec = parent.getQueryRec();
            //String record = parent.getQueryRec();
            if (ingestRec == null)
            {
                log.info("Query thread " + name + " stopping.");
                break;
            }

            sw.start();
            query(ingestRec.getAccountToken());
            long timeStamp = System.currentTimeMillis();
            long diff = timeStamp - ingestRec.getTimestamp();
            availabilityTotal += diff;
            //log.debug("Availability time: " + diff + "ms.");
            sw.stop();

            totalMS = totalMS.add(BigInteger.valueOf(sw.elapsed(TimeUnit.MILLISECONDS)));
            totalRecords++;
            sw.reset();
        }
    }

    private void query(String rec)
    {
        try
        {
            int retries = 0;
            final int MAX_RETRIES = 10;
            URL recordQuery = new URL(queryEndpoint.toString() + "?q=account_token:" + rec);
            for (int i = 0; i < MAX_RETRIES; i++)
            {
                // TODO may be possible to reduce network I/O by adding a filter query fq
                con = (HttpURLConnection) recordQuery.openConnection();
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", authHeader);
                con.setRequestProperty("Accept", "application/json");
                con.setConnectTimeout(10000);
                con.setReadTimeout(10000);
                con.setInstanceFollowRedirects(false);
                int resultCode = con.getResponseCode();

                // Get contents of response
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line = "";
                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null)
                {
                    sb.append(line);
                }

                br.close();
                con.disconnect();

                // Extract number of results
                JSONObject jo = new JSONObject(sb.toString());
                JSONObject resp = (JSONObject) jo.get("response");
                int num = (int) resp.get("numFound");
                int expected = 0;

                if (num >= 1)
                {
                    JSONArray docs = (JSONArray)(resp.get("docs"));
                    expected = (int) ((JSONObject)docs.get(0)).get("tx_count");
                }

                if (num == 0)
                {
                    log.warn("Mismatch: Customer record " + rec + " returned 0 results. Retrying query.");
                }
                else if (num != expected)
                {
                    log.warn("Mismatch: Customer record " + rec + " returned " + num + " of " + expected + " expected. Retrying query.");
                }

                if ((num == 0 || num != expected) && retries == MAX_RETRIES - 2)
                {
                    log.error("Maximum number of retries reached for customer " + rec);
                    retries++;
                    queryFailed++;
                    break;
                }

                if (retries > 0)
                {
                    log.info("Number of results for account_token " + rec + ": " + num + " (" + retries + " previous attempts)");
                }

                if (num == 0 || (num >= 1 && (num < expected)))
                {
                    retries++;
                    //Thread.sleep(1000 + (((retries + 1) * 3) * 1000));
                    Thread.sleep(backoff);
                    continue;
                }

                transactionsFound += num;
                if (resultCode >= 200 && resultCode <= 299)
                {
                    querySucceeded++;
                }
                else
                {
                    queryFailed++;
                    log.error("Query failed. Status code: " + resultCode);
                }
                break;
            }

            numRetries += retries;

        }
        catch (IOException io)
        {
            log.error("Error during query: " + io.getMessage());
            queryFailed++;
        }
        catch (InterruptedException ie)
        {
            log.error("Unexpected interruption from query worker: " + ie.getMessage());
            queryFailed++;
        }
    }

    public int getSucceeded()
    {
        return querySucceeded;
    }

    public int getFailed()
    {
        return queryFailed;
    }

    public long getTotalTransactions()
    {
        return transactionsFound;
    }

    public int getNumRetries()
    {
        return numRetries;
    }

    public int getAverageTime()
    {
        return (totalMS.divide(BigInteger.valueOf(totalRecords))).intValue();
    }

    public long getTotalAvailability()
    {
        return availabilityTotal;
    }
}
