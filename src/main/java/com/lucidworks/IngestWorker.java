package com.lucidworks;

import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.util.concurrent.TimeUnit;

public class IngestWorker implements Runnable
{
    private Logger log;
    private String name;
    private URL ingestEndpoint;
    private IngestHandler parent;
    private boolean success;
    private int ingestSuccess;
    private int ingestFailed;
    private Stopwatch sw;
    private BigInteger totalMS;
    private long totalRecords;
    private String authHeader;
    private QueryHandler queryHandler;
    private HttpURLConnection con;

    public IngestWorker(String name, String ingestEndpoint, IngestHandler parent, QueryHandler queryHandler, String authHeader) throws MalformedURLException
    {
        log = LogManager.getLogger(IngestWorker.class);
        this.name = name;
        this.ingestEndpoint = new URL(ingestEndpoint);
        this.parent = parent;
        this.queryHandler = queryHandler;
        this.authHeader = authHeader;
        success = false;
        ingestFailed = 0;
        ingestSuccess = 0;
        sw = Stopwatch.createUnstarted();
        totalMS = BigInteger.valueOf(0);
        totalRecords = 0;
    }

    public void run()
    {
            log.info("Started ingest thread " + name + ".");

            while (true)
            {
                CustomerRecord record = parent.getPayload();
                if (record == null) {
                    log.info("Ingest thread " + name + " stopping.");
                    break;
                }
                sw.start();
                ingest(record);
                sw.stop();
                //log.debug(name + " ingested record in " + sw.elapsed(TimeUnit.MILLISECONDS) + " ms.");
                totalMS = totalMS.add(BigInteger.valueOf(sw.elapsed(TimeUnit.MILLISECONDS)));
                totalRecords++;
                sw.reset();
            }

    }

    private void ingest(CustomerRecord record)
    {
        try
        {
            con = (HttpURLConnection) ingestEndpoint.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", authHeader);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setConnectTimeout(10000);
            con.setReadTimeout(10000);
            con.setInstanceFollowRedirects(false);
            con.setDoOutput(true);
            con.getOutputStream().write(record.getRecord().getBytes(StandardCharsets.UTF_8));
            con.getOutputStream().close();
            InputStream is = con.getInputStream();
            while (is.read() != -1)
            {
                // no op - just make sure the input stream is fully read before disconnect or socket will not be reused
                // See https://stackoverflow.com/questions/9943351/httpsurlconnection-and-keep-alive
            }
            is.close();

            int resultCode = con.getResponseCode();
            con.disconnect();  //counterintuitively, disconnect must be called in the loop for the underlying socket to be reused
            if (resultCode >= 200 && resultCode <= 299)
            {
                ingestSuccess++;
                long timestamp = System.currentTimeMillis();
                queryHandler.addRecord(new IngestedRecord(record.getAccountToken(), timestamp));
            }
            else
            {
                ingestFailed++;
                log.warn("Ingest failed. Status code: " + resultCode);
            }
        }
        catch (IOException e)
        {
            ingestFailed++;
            log.error("Ingest Failed: " + e.getMessage());
        }
    }

    public int getSucceeded()
    {
        return ingestSuccess;
    }

    public int getFailed()
    {
        return ingestFailed;
    }

    public int getAverageTime()
    {
        return (totalMS.divide(BigInteger.valueOf(totalRecords))).intValue();
    }

}
