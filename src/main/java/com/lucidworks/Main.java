package com.lucidworks;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Main
{
    public static void main(String[] args)
    {
        System.setProperty("logfile.name", "amexTxnGen.log");

        int numRecordsWanted = 5000;
        int numRecordsPerCustomerAvg = 50;
        int numThreads = 8;
        String outputDir = "";
        String ingestEndpoint = "";
        String queryEndpoint = "";
        Logger log = LogManager.getLogger(Main.class);
        int delaySeconds = 15 * 60;
        boolean dryRun = false;
        String authHeader = "";
        int backoff = 1000;

        Options options = new Options();
        try
        {
            options.addOption("o", "outDir", true, "Optional output directory for data generation.");
            options.addOption("n", "numRecords", true, "Number of records to generate. Default 5000.");
            options.addOption("p", "recordsPerCustomer", true, "Average number of records per customer. Default 50.");
            options.addOption("i", "ingestEndpoint", true, "REST endpoint for ingest pipeline.");
            options.addOption("q", "queryEndpoint", true, "REST endpoint for query pipeline.");
            options.addOption("t", "threads", true, "Number of threads for ingest and query. Default 8.");
            options.addOption("d", "delay", true, "Duration in seconds to wait after beginning ingest to start querying for data. Default 900s (15 minutes.)");
            options.addOption("basicAuthUsername", true, "Username for basic authentication.");
            options.addOption("basicAuthPassword", true, "Password for basic authentication.");
            options.addOption("j", "jwt", true, "JWT token to use for authentication.");
            options.addOption("b", "backoff", true, "Query retry delay, in ms. Default 1000.");
            CommandLineParser parser = new DefaultParser();

            CommandLine cmd = parser.parse(options, args);
            if (cmd.getOptions().length == 0)
            {
                System.out.println("No arguments provided!");
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("amexTxnGen [options] outDir", options);
                System.exit(1);
            }

            if (cmd.hasOption('o'))
            {
                outputDir = cmd.getOptionValue('o');
            }

            if (cmd.hasOption('n'))
            {
                numRecordsWanted = Integer.parseInt(cmd.getOptionValue('n'));
            }

            if (cmd.hasOption('p'))
            {
                numRecordsPerCustomerAvg = Integer.parseInt(cmd.getOptionValue('p'));
            }

            if (cmd.hasOption('t'))
            {
                numThreads = Integer.parseInt(cmd.getOptionValue('t'));
            }

            if (cmd.hasOption('i'))
            {
                ingestEndpoint = cmd.getOptionValue('i');
            }

            if (cmd.hasOption('q'))
            {
                queryEndpoint = cmd.getOptionValue('q');
            }

            if (cmd.hasOption('d'))
            {
                delaySeconds = Integer.parseInt(cmd.getOptionValue('d'));
                if (delaySeconds < 0)
                {
                    delaySeconds = 0;
                }
            }

            if (cmd.hasOption('b'))
            {
                backoff  = Integer.parseInt(cmd.getOptionValue('b'));
                if (backoff < 0)
                {
                    backoff = 50;
                    log.warn("Backoff value must be at least 50 ms. Setting to 50.");
                }
            }

            if ((ingestEndpoint == "" || queryEndpoint == "") && (outputDir != ""))
            {
                log.warn("Both ingest and query endpoints must be defined to perform benchmark test." +
                        " This invocation of the application will only generate data.");
                dryRun = true;
            }
            else if ((ingestEndpoint == "" || queryEndpoint == "") && (outputDir == ""))
            {
                log.error("Please specify either an ingest and query endpoint, or an output directory to store generated data.");
                System.exit(1);
            }
            else
            {
                log.info("Performing benchmark test. New documents will be submitted to " + ingestEndpoint + " and queried from " + queryEndpoint);
                if (cmd.hasOption("basicAuthUsername") && cmd.hasOption("basicAuthPassword"))
                {
                    authHeader = genBasicAuthHeader(cmd.getOptionValue("basicAuthUsername"), cmd.getOptionValue("basicAuthPassword"));
                }
                else if (cmd.hasOption('j'))
                {
                    authHeader = genJWTHeader(cmd.getOptionValue('j'));
                }
                else
                {
                    log.error("Either basic authentication credentials or a JWT token must be specified to perform benchmark tests.");
                    System.exit(1);
                }
            }

            if (dryRun)
            {
                DataGenerator gen = new DataGenerator(numRecordsWanted, numRecordsPerCustomerAvg, outputDir);
                gen.generate();
            }
            else
            {
                // DataGenerator -> IngestHandler -> QueryHandler. Each calls the next's "setJobFinished" method to set appropriate completion flags.
                // This allows for coordination of timing and prevents app shutdown before all 3 stages are complete
                DataGenerator gen = new DataGenerator(numRecordsWanted, numRecordsPerCustomerAvg, outputDir);
                QueryHandler query = new QueryHandler(queryEndpoint, numThreads, delaySeconds, backoff, authHeader);
                IngestHandler ingest = new IngestHandler(ingestEndpoint, numThreads, query, authHeader, gen);

                //gen.generate();
            }
            //log.info("System exiting.");

            //System.exit(0); // just in case we have a hung up thread somewhere
        }
        catch (ParseException p)
        {
            System.out.println("Error parsing command line options: " + p.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("amexTxnGen [options] outDir", options);
            System.out.println("If a custom CA certificate is required, specify truststore JKS with the -Djavax.net.ssl.trustStore argument.");
            System.exit(1);
        }
    }

    public static String genJWTHeader(String jwt)
    {
        String token = "Bearer " + jwt;
        return token;
    }

    public static String genBasicAuthHeader(String username, String pass)
    {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + pass).getBytes(StandardCharsets.UTF_8));
    }
}
