package com.lucidworks;

// Another simple datatype
public class IngestedRecord
{
    private String accountToken;
    private long timestamp;

    public IngestedRecord(String accountToken, long timestamp)
    {
        this.accountToken = accountToken;
        this.timestamp = timestamp;
    }

    public String getAccountToken()
    {
        return accountToken;
    }

    public long getTimestamp()
    {
        return timestamp;
    }
}
