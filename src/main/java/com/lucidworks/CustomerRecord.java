package com.lucidworks;

// A simple data structure for pairing an account token with the full text of a record.
// Saves us a dependency on a tuple library
public class CustomerRecord
{
    private String accountToken;
    private String record;

    public CustomerRecord(String accountToken, String record)
    {
        this.accountToken = accountToken;
        this.record = record;
    }

    public String getAccountToken()
    {
        return accountToken;
    }

    public String getRecord()
    {
        return record;
    }
}
