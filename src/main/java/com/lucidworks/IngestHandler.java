package com.lucidworks;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.Thread.sleep;

public class IngestHandler
{
    private URL ingestEndpoint;
    //private BlockingQueue<CustomerRecord> records;
    private ExecutorService executor;
    private List<Runnable> workers;
    private Semaphore availablePayload;
    protected Logger log;
    private boolean stopping;
    private Stopwatch swIngest;
    private QueryHandler queryHandler;
    private int numRecordsGenerated;
    private int numCustomersGenerated;
    private String authHeader;
    private DataGenerator dataGen;

    public IngestHandler(String endpoint, int numThreads, QueryHandler queryhandler, String authHeader, DataGenerator dataGen)
    {
        log = LogManager.getLogger(IngestHandler.class);
        executor = Executors.newFixedThreadPool(numThreads);
        workers = new ArrayList<>();
        availablePayload = new Semaphore(0, true);
        //records = new LinkedBlockingQueue<>();
        stopping = false;
        swIngest = Stopwatch.createUnstarted();
        this.queryHandler = queryhandler;
        this.numCustomersGenerated = 0;
        this.numRecordsGenerated = 0;
        this.authHeader = authHeader;
        this.dataGen = dataGen;

        try
        {
            ingestEndpoint = new URL(endpoint);
            log.info("Started IngestHandler for " + endpoint + " with " + numThreads + " threads.");

            if (!verifyEndpoint())
            {
                log.error("Unable to verify ingest endpoint. Application will now exit.");
                System.exit(1);
            }

            swIngest.start();

            for (int i = 0; i < numThreads; i++)
            {
                Runnable worker = new IngestWorker("Ingest" + i, endpoint, this, queryHandler, authHeader);
                workers.add(worker);
                executor.execute(worker);
            }
        }
        catch (MalformedURLException ue)
        {
            log.error("Unable to parse ingest URL: " + endpoint + ". Error: " + ue.getMessage());
            System.exit(1);
        }
    }

    // External so data gen can call as soon as it sends over the first record
    public void startTimer()
    {
        swIngest.start();
    }

//    public void addRecord(String accountToken, String rec)
//    {
//        records.add(new CustomerRecord(accountToken, rec));
//        availablePayload.release();
//    }

    public synchronized CustomerRecord getPayload()
    {
//        if (records.isEmpty() && stopping) return null;
//        try
//        {
//            availablePayload.acquire();
//            return records.take();
//        }
//        catch (InterruptedException e)
//        {
//            // InterruptedExceptions can only mean the app is closing - so just exit
//            log.error(e.getMessage());
//            System.exit(1);
//        }
//
//        //unreachable
//        return null;
        if (stopping) return null;
        CustomerRecord rec = dataGen.generateOne();
        if (rec == null)
        {
            this.setJobFinished(dataGen.getNumCustomersGenerated(), dataGen.getNumRecordsGenerated());
        }
        return rec;
    }

    public void setJobFinished(int numCustomersGenerated, int numRecordsGenerated) {
        stopping = true;
        this.numRecordsGenerated = numRecordsGenerated;
        this.numCustomersGenerated = numCustomersGenerated;

        // Wait for the generated records queue to empty
        // An event-drive approach would be nice but isn't necessary
//        while (true)
//        {
//            try
//            {
//                if (records.isEmpty()) break;
//                else
//                {
//                    sleep(1000);
//                }
//            }
//            catch (InterruptedException e)
//            {
//                //no-op - just continue
//            }
//        }
        try
        {
            sleep(1000);
        }
        catch (InterruptedException e)
        {
            //no-op
        }

        swIngest.stop();
        log.info("Waiting for ingest threads to finish...");
        executor.shutdown();
        try
        {
            // One set of records per thread left at this point - 60 seconds should be more than enough
            executor.awaitTermination(60, TimeUnit.SECONDS);
        }
        catch (InterruptedException ie)
        {
            log.error("Encountered error during ingest worker shutdown: " + ie.getMessage());
        }

        int totalSuccess = 0;
        int totalFailure = 0;
        int totalTimes = 0;
        for (Runnable w : workers)
        {
            totalSuccess += ((IngestWorker)w).getSucceeded();
            totalFailure += ((IngestWorker)w).getFailed();
            totalTimes += ((IngestWorker)w).getAverageTime();
        }

        // Passing ingest stats to query handler so all can be output at the same time after the run is over.
        // This "pass everything around" architecture is beginning to buckle under its own weight, suggest
        // writing a central collector if any further customization is needed

        double averageIngest = totalTimes / workers.size();
        double docsPerSecond = (double)numRecordsGenerated / swIngest.elapsed(TimeUnit.SECONDS);
        double customersPerSecond = (double)numCustomersGenerated / swIngest.elapsed(TimeUnit.SECONDS);

        StringBuilder sb = new StringBuilder();
        log.info("Ingest workers have finished.");
        sb.append("Ingest workers terminated. Total successful customers ingested: " + totalSuccess + ". Total failures: " + totalFailure + ". Elapsed time: " + swIngest.elapsed().getSeconds() + " seconds.\n");
        sb.append("Ingest documents per second: " + String.format("%1$.2f", docsPerSecond) + ", customers per second: " + String.format("%1$.2f", customersPerSecond) + "\n");
        sb.append("Average ingest time for single customer: " + averageIngest + " ms.\n");

        // Let the queryHandler know that we're done sending over records.
        queryHandler.setJobFinished(numCustomersGenerated, numRecordsGenerated, sb.toString());
    }

    private boolean verifyEndpoint()
    {
        try
        {
            // Send an empty POST to the endpoint and verify response code 200
            HttpURLConnection con = (HttpURLConnection) ingestEndpoint.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Authorization", authHeader);

            con.setRequestProperty("Content-Type", "application/json");
            con.setConnectTimeout(10000);
            con.setReadTimeout(10000);
            con.setInstanceFollowRedirects(false);
            int status = con.getResponseCode();
            InputStream is = con.getInputStream();
            while (is.read() != -1) { }
            is.close();
            con.disconnect();
            if (status >= 200 && status <= 299)
            {
                log.info("Ingest endpoint verified.");
                return true;
            }
            else
            {
                log.error("Unexpected response from ingest endpoint: HTTP " + status);
                return false;
            }
        }
        catch (IOException e)
        {
            log.error("Invalid ingest URL: " + e.getMessage());
            return false;
        }
    }

}
