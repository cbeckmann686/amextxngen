package com.lucidworks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataGenerator
{
    private int numRecordsWanted;
    private int numRecordsGenerated;
    private int numCustomersGenerated;
    private int numRecordsPerCustomerAvg;
    private String outputDir;
    private File outDir = null;
    //private IngestHandler ingest;
    //private QueryHandler query;
    protected Logger log;
    private String[] phrases;
    private Random r;

    //TODO: Is a single thread for data generation fast enough? Because there is no direct I/O, I believe it is.
    public DataGenerator(int numRecordsWanted, int numRecordsPerCustomerAvg, String outputDir)
    {
        this.numRecordsWanted = numRecordsWanted;
        numRecordsGenerated = 0;
        numCustomersGenerated = 0;
        this.numRecordsPerCustomerAvg = numRecordsPerCustomerAvg;
        this.outputDir = outputDir;
        //this.ingest = ingest;
        //this.query = query;
        phrases = Phrases.getPhrases();
        r = new Random();
        //spin up PRNG
        for (int i = 0; i < 500000; i++)
        {
            r.nextInt();
        }
        log = LogManager.getLogger(DataGenerator.class);
        log.info("Started data generator. Target: " + numRecordsWanted + " transaction records averaging " + numRecordsPerCustomerAvg + " per customer.");
        if (outputDir == "")
        {
            log.info("Records will not be saved to disk.");
        }
        else
        {
            log.info("Records will be saved to disk in directory " + outputDir + ".");
        }
    }

    public CustomerRecord generateOne()
    {
        // A little bit of redundancy in this method
        if (getNumRecordsGenerated() < numRecordsWanted)
        {
            String accountId = getAccountToken(r);
            int min = (int) Math.floor(numRecordsPerCustomerAvg * .5);
            int max = (int) Math.floor(numRecordsPerCustomerAvg * 1.77);
            int recordsForCustomer = min + (int) (Math.random() * ((max - min) + 1));
            numRecordsGenerated = getNumRecordsGenerated() + recordsForCustomer;
            numCustomersGenerated++;

            StringBuilder customerRecs = new StringBuilder();
            customerRecs.append("{\n\"records\": [");  //open json

            for (int i = 0; i < recordsForCustomer; i++)
            {
                String rec = genRecord(r, phrases, accountId);
                customerRecs.append(rec);
                if (i <= recordsForCustomer - 2)  // Omit comma on last record for this customer
                {
                    customerRecs.append(",\n");
                }
            }

            customerRecs.append("]}");  //close json
            return new CustomerRecord(accountId, customerRecs.toString());
        }
        else
        {
            //ingest.setJobFinished(numCustomersGenerated, numRecordsGenerated);
            return null;
        }
    }

    public void generate()
    {
        if (outputDir != "")
        {
            createOutputDir();
        }

        numRecordsGenerated = 0;
        numCustomersGenerated = 0;

        // Start ingest stopwatch just before submitting first record
//        if (ingest != null && query != null)
//        {
//            ingest.startTimer();
//            query.startTimer();
//        }

        while (getNumRecordsGenerated() < numRecordsWanted)
        {
            String accountId = getAccountToken(r);
            int min = (int) Math.floor(numRecordsPerCustomerAvg * .5);
            int max = (int) Math.floor(numRecordsPerCustomerAvg * 1.77);
            int recordsForCustomer = min + (int) (Math.random() * ((max - min) + 1));

            numRecordsGenerated = getNumRecordsGenerated() + recordsForCustomer;
            numCustomersGenerated++;

            StringBuilder customerRecs = new StringBuilder();
            customerRecs.append("{\n\"records\": [");  //open json

            for (int i = 0; i < recordsForCustomer; i++)
            {
                String rec = genRecord(r, phrases, accountId);
                customerRecs.append(rec);
                if (i <= recordsForCustomer - 2)  // Omit comma on last record for this customer
                {
                    customerRecs.append(",\n");
                }
            }

            customerRecs.append("]}");  //close json

//            if (ingest != null && query != null)
//            {
//                ingest.addRecord(accountId, customerRecs.toString());
//                // adding query records to the queue here creates a race condition
//                // Instead, have ingestworkers add them synchronously to the query queue after ingestion.
//                //query.addRecord(accountId);
//            }

            if (outputDir != "")
            {
                try (FileWriter writer = new FileWriter(outDir.getAbsolutePath() + "/" + accountId + ".json", false);)
                {
                    writer.write(customerRecs.toString());
                }
                catch (IOException e)
                {
                    log.error("Error writing file: " + e.getMessage());
                    System.exit(1);
                }
            }
        }

        log.info("Generated " + getNumRecordsGenerated() + " records for " + numCustomersGenerated + " customers.");
        //if (ingest != null) ingest.setJobFinished(numCustomersGenerated, numRecordsGenerated);
    }

    private void createOutputDir()
    {
        // Create dir if needed
        if (outputDir != "")
        {
            outDir = new File(outputDir);
            if (!outDir.exists())
            {
                if (outDir.mkdirs())
                {
                    log.info("Created output directory: " + outputDir);
                } else
                {
                    log.error("Unable to create output directory!");
                    System.exit(1);
                }
            }
        }
    }

    private String genRecord(Random r, String[] phrases, String accountToken)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");

        //Account Token
        if (accountToken != null && accountToken.length() > 0)
        {
            sb.append("\"account_token\": ");
            sb.append("\"" + accountToken + "\"");
            sb.append(",\n");
        }
        else
        {
            sb.append("\"account_token\": ");
            sb.append("\"" + getAccountToken(r) + "\"");
            sb.append(",\n");
        }

        //Reference Number
        sb.append("\"reference_number\": ");
        sb.append("\"" + getReferenceNumber(r) + "\"");
        sb.append(",\n");

        //Amount
        sb.append("\"amount\": ");
        sb.append(getAmount(r));
        sb.append(",\n");

        //Charge Date
        sb.append("\"charge_date\": ");
        sb.append("\"" + getDate(r) + "\"");
        sb.append(",\n");

        //Statement end date
        sb.append("\"statement_end_date\": ");
        sb.append("\"" + getDate(r) + "\"");
        sb.append(",\n");

        //transaction type
        sb.append("\"transaction_type\": ");
        sb.append("\"" + getTxType(r) + "\"");
        sb.append(",\n");

        //transaction sub-type
        sb.append("\"transaction_sub_type\": ");
        sb.append("\"" + getTxSubType(r) + "\"");
        sb.append(",\n");

        //supp digits
        sb.append("\"supp_digits\": ");
        sb.append("\"" + getSuppDigits(r) + "\"");
        sb.append(",\n");

        //Description
        sb.append("\"description\": ");
        sb.append("\"" + getDescription(r, phrases) + "\"");
        sb.append(",\n");

        //se number
        sb.append("\"se_number\": ");
        sb.append("\"" + getSeNumber(r) + "\"");
        sb.append(",\n");

        //Merchant name
        sb.append("\"merchant_name\": ");
        sb.append("\"" + getMerchant(r) + "\"");
        sb.append(",\n");

        //category id
        sb.append("\"category_id\": ");
        sb.append("\"" + getCategoryNum(r) + "\"");
        sb.append(",\n");

        //sub_category_id
        sb.append("\"sub_category_id\": ");
        sb.append("\"" + getCategoryNum(r) + "\"");
        sb.append(",\n");

        //tag ids (up to 10)
        sb.append("\"tag_ids\": ");
        sb.append("[\n" + getTagIds(r) + "\n]\n");

        sb.append("}");
        return sb.toString();
    }

    private String getAccountToken(Random r)
    {
        // ex. JY86QO20NV6BWCH
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(15);
        for (int i = 0; i < 15; i++)
        {
            sb.append(chars[r.nextInt(chars.length)]);
        }
        return sb.toString();
    }

    private String getReferenceNumber(Random r)
    {
        // ex. 110427230000033519
        char[] nums = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder(18);
        for (int i = 0; i < 18; i++)
        {
            sb.append(nums[r.nextInt(nums.length)]);
        }
        return sb.toString();
    }

    private String getAmount(Random r)
    {
        return String.format("%1$.2f", 10.0 + r.nextDouble() * (1000.0 - 10.0));
    }

    private String getDate(Random r)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("2021-");
        sb.append(r.nextInt(11) + 1);
        sb.append("-");
        sb.append(r.nextInt(27) + 1);
        return sb.toString();
    }

    private String getTxType(Random r)
    {
        return (r.nextInt(2) >= 1) ? "DEBIT" : "CREDIT";
    }

    public static String getTxSubType(Random r)
    {
        // Always "OTHER" in the samples provided
        return "OTHER";
    }

    public static String getSuppDigits(Random r)
    {
        // Always 00 in the samples provided
        return "00";
    }

    public static String getDescription(Random r, String[] phrases)
    {
        return phrases[r.nextInt(phrases.length)];
    }

    public static String getSeNumber(Random r)
    {
        // Always 0000000000 in the samples provided
        return "0000000000";
    }

    public static String getMerchant(Random r)
    {
        return "AMERICAN EXPRESS INTERNAL TRANSACTION";
    }

    public static String getCategoryNum(Random r)
    {
        //2 digit hex, looks like
        char[] chars = "0123456789ABCDEF".toCharArray();
        return "" + chars[r.nextInt(chars.length)] + chars[r.nextInt(chars.length)];
    }

    public static String getTagIds(Random r)
    {
        StringBuilder sb = new StringBuilder();
        int numIds = r.nextInt(10) + 1;
        for (int i = 0; i < numIds; i++)
        {
            sb.append("\"");
            sb.append(r.nextInt(1000000));
            sb.append("\"");
            if (i != numIds - 1) sb.append(",\n");
        }
        return sb.toString();
    }

    public int getNumRecordsGenerated()
    {
        return numRecordsGenerated;
    }

    public int getNumCustomersGenerated()
    {
        return numCustomersGenerated;
    }
}
