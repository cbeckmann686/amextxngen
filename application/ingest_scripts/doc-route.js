function(doc, ctx)
{
    var BODY_FIELD = "body";
    var NUM_HOUR_COLLECTIONS = 4;
    var META_COLLECTION_NAME = "amex_txn_meta";
    var ID_FIELD = "id";
    var COLLECTION_ID_FIELD = "collection_id"
    var CUSTOMER_TOKEN_FIELD = "account_token";
    var TX_ID_FIELD = "reference_number";
    var HOUR_COLLECTION_PREFIX = "hour";

    // Why body_t? We're skipping Fusion's built-in json parser for speed and record size.
    if (doc.hasField(BODY_FIELD)) {
        //logger.info("body_t found: " + doc.getFirstFieldValue("body_t"));
    } else {
        logger.warn("body field missing - aborting.")
        return null;
    }

    var JSONObject = Java.type("org.json.JSONObject");
    var JSONArray = Java.type("org.json.JSONArray");
    var ArrayList = Java.type("java.util.ArrayList");
    var PipelineDocument = Java.type("com.lucidworks.apollo.common.pipeline.PipelineDocument");

    var fullJson = new JSONObject(doc.getFirstFieldValue(BODY_FIELD));
    if (!fullJson.has("records")) {
        logger.error("Unable to locate records key! Aborting.")
        return null;
    }
    
    var recordsJson = fullJson.get("records");  //works
    var docs = new ArrayList();

    // Determine hour % 24 to determine collection
    // Important to do this once, not in the loop - so all our transactions end up in the same collection
    var today = new Date();
    var hour = today.getHours();
    var collectionHour = hour % NUM_HOUR_COLLECTIONS;
    var collectionName = HOUR_COLLECTION_PREFIX + collectionHour;
    //logger.info("Routing document " + custId + " to collection " + collectionHour);
    //route to appropriate collection
    
    ctx.collection = collectionName;

    // Extract customer ID - needed for shard routing
    // Grab custId once instead of each time through the loop
    var custId = null;
    var firstTx = recordsJson.get(0);
    if (firstTx.has(CUSTOMER_TOKEN_FIELD)) {
        custId = firstTx.get(CUSTOMER_TOKEN_FIELD);
        //logger.info("Found account token: " + custId);
    } else {
        logger.info("Account token field missing, aborting!");
        return null;
    }

    for (var i = 0; i < recordsJson.length(); i++) {
        //logger.info("tx " + i + ": " + recordsJson.get(i));
        var txJson = recordsJson.get(i);

        var txId = null;
        if (txJson.has(TX_ID_FIELD)) {
            txId = txJson.get(TX_ID_FIELD);
            //logger.info("Transaction ID: " + txId);
        } else {
            logger.info("Transaction ID field missing, skipping document...");
            continue;
        }

        var transaction = new PipelineDocument(custId);
        transaction.setField(ID_FIELD, custId + "!" + txId);
        transaction.setField(CUSTOMER_TOKEN_FIELD, custId);
        transaction.setField(COLLECTION_ID_FIELD, collectionHour);
        transaction.addField("record_type", "tx");
        transaction.addField("reference_number", recordsJson.get(i).get("reference_number"));
        transaction.addField("amount", recordsJson.get(i).get("amount"));
        transaction.addField("charge_date", recordsJson.get(i).get("charge_date"));
        transaction.addField("statement_end_date", recordsJson.get(i).get("statement_end_date"));
        transaction.addField("transaction_type", recordsJson.get(i).get("transaction_type"));
        transaction.addField("transaction_sub_type", recordsJson.get(i).get("transaction_sub_type"));
        transaction.addField("supp_digits", recordsJson.get(i).get("supp_digits"));
        transaction.addField("description", recordsJson.get(i).get("description"));
        transaction.addField("se_number", recordsJson.get(i).get("se_number"));
        transaction.addField("merchant_name", recordsJson.get(i).get("merchant_name"));
        transaction.addField("category_id", recordsJson.get(i).get("category_id"));
        transaction.addField("sub_category_id", recordsJson.get(i).get("sub_category_id"));
        transaction.addField("tag_ids", recordsJson.get(i).get("tag_ids"));
        transaction.addField("tx_count", recordsJson.length());
        docs.add(transaction);
    }

    //logger.info("Number of transactions for customer " + custId + ": " + docs.size());

    // Create meta-collection entry
    var metaRecord = new com.lucidworks.apollo.common.pipeline.PipelineDocument(custId);
    metaRecord.addField("account_token", custId);
    metaRecord.addField("collection_name", collectionName);
    metaRecord.addField("tx_count", docs.size());
    metaRecord.addField("record_type", "meta");  //TODO make boolean instead of string
    //logger.info("Created meta record: customer id " + custId + " indexed in collection " + collectionName);
    docs.add(metaRecord);

    return docs;
}