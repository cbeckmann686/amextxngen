/* globals Java, logger*/
(function () {
  "use strict";
   var SolrQuery = Java.type('org.apache.solr.client.solrj.SolrQuery');

  var IOUtils = Java.type('org.apache.commons.io.IOUtils');
  var PipelineDocument = Java.type('com.lucidworks.apollo.common.pipeline.PipelineDocument');
  var PipelineField = Java.type('com.lucidworks.apollo.common.pipeline.PipelineField');

  var libName = 'SolrJHelper'
  var lib = {};
  var isDebug = false;
  lib.setDebugLogging = function(debug_b){
    isDebug = (true && debug_b);
  }
  lib.logIfDebug = function(message){
    if( isDebug){
      logger.info(Array.prototype.slice.call(arguments));
    }
  }



    /**
     *
     * @param collectionName  name of the Fusion collection being queried by Solr.  If the Fusion collection references
     * a Solr Collection on another cluster or with a different name, that collection will be associated with the returned
     * SolrServer such that calls which do not include the optional Solr Collection name will be executed against the target
     * of the Fusion collection.  In other words, calling solrServer.query(solrQuery) will execute against  the Solr collection
     * referenced by the Fusion collection passed in collectionName.  Calling solrServer.query(solrCollection,solrQuery)
     * will execute against the Solr Collection `solrCollection`
     *
     * @param solrServerFactory  The Fusion Factory create/get the SolrServer objects.  This is a pipeline stage parameter
     * @returns solrClient
     */
    lib.makeSolrServer = function(collectionName, solrServerFactory){
        var SolrServer = solrServerFactory.getSolrServer(collectionName);
        return SolrServer
    }


    /**
     * execute an existing SolrQuery returning a QueryResponse object.  Use response.getResults() to get a
     * SolrDocumentList (ArrayList<SolrDocument>).  Which can be itterated or accessed as needed e.g. resp.getNmFound()

     * @param query Solr query i.e. '*:*'
     * @param solrClient
     * @return  the org.apache.solr.client.solrj.response.UpdateResponse or null if there was an error
     *  @see http://lucene.apache.org/solr/4_8_1/solr-solrj/org/apache/solr/client/solrj/response/QueryResponse.html

     */
    lib.querySolr = function(solrQuery, solrServer) {
        try {
            lib.logIfDebug("{} doing solr query:{}",libName,solrQuery);
            var resp = solrServer.query(solrQuery);
            /*
             var cnt = 0;
             var message = '';
             resp.getResults().forEach(function (respDoc) {
             message += ('\n\tmessage_t for result ' + (cnt++) + ' is: ' + respDoc.getFirstValue('message_t') );
             });
             logger.info('\n\nMessages: ' + message);
             */
            lib.logIfDebug("{} got reponse: {}",libName,resp)
            return resp;
        } catch (error) {
            logger.error("Error querying solr for '" + solrQuery + "' Err: " + error);
        }
        return null;
    };
    /**
     * Clone a PipelineDocument
     * @param pipelineDoc
     * @param id  optional id.  If valued, the clone will have this set
     */
    lib.clonePipelineDoc = function(pipelineDoc, id){
        var clone = new PipelineDocument(pipelineDoc);
        if(id){
            clone.setId(id);
        }
        return clone;
    };
    /**
     * Transfer fields from an org.apache.solr.common.SolrDocument into a PipelineDocument
     *
     * @param doc {PipelineDocument}
     * @param solrDoc {org.apache.solr.common.SolrDocument}
     * @param fieldMap a hash {} of solrDoc fieldNames : pipelineDocument fieldnames.
     */
    lib.copyFieldsFromSolr = function(doc, solrDoc, fieldMap){
        for( var solrFieldName in fieldMap){
            var pipelineFieldName = fieldMap[solrFieldName] || solrFieldName;
            lib.logIfDebug('* copying Solr field: {} to: {}', solrFieldName ,pipelineFieldName);

            if(solrDoc.containsKey(solrFieldName)){
              doc.addField(pipelineFieldName, solrDoc.get(solrFieldName));
                lib.logIfDebug('added mapped field from PipelineDoc{} : SolrDoc{}', pipelineFieldName ,solrFieldName)
            }else{
                logger.warn('Could not find field "' + solrFieldName + '" in solrDocument with fields: ' + solrDoc.getFieldNames());
            }
        }
    };
     /**
     * Delete a known document from Solr.
     *
     * Note:  unless you call some form of client.commit()  you may not see this for a while.
     *
     * @param solrClient The SolrJ client AKA solrServer
     * @param id
     * @return  the org.apache.solr.client.solrj.response.UpdateResponse
     */
    lib.deleteById = function(id,solrClient,commitWithin){
        commitWithin = commitWithin || 30000
        var response;
        try {
            if(solrClient) {
                response = solrClient.deleteById(id,30000);
                //check response
                if(response.getStatus() != 0){
                    logger.warn("got non 0 status (" + response.getStatus() + ") when attempting to delete Solr document " + id);
                }
            }
        }catch (error) {
            logger.error("Error deleting from Solr for id:'" + id + "'" + error);
        }
        return response;
    };

    // Added for Amex 10/20/2021 CB
    lib.deleteByQuery = function(query, solrClient, commitWithin){
        commitWithin = commitWithin || 30000;
        var response;
        try {
            response = solrClient.deleteByQuery(query);
            solrClient.commit();
            if (response.getStatus() != 0) {
                logger.warn("Got nonzero status (" + response.getStatus() + ") when attempting to delete by query.");
            }
        }catch (error) {
            logger.error("Error deleting from solr by query: " + error);
        }
        return response;
    }

    /**
      Lazy load this `lib` into the ctx object.
      If called as a Managed Index Pipeline, the arguments will be doc, ctx, collection, solrClient, solrFactory
      If called as a Managed Query Pipeline, the arguments will be request, response, ctx...

      Check for argument types to figure out which one is the ctx.

    */

    return function () {
      var args = Array.prototype.slice.call(arguments);
      var rtnObj = null
      //var Context = Java.type("com.lucidworks.apollo.pipeline.impl.DefaultContext")
      var Context = Java.type("com.lucidworks.apollo.pipeline.Context")


      //var name1 = args[1].getClass().getName()
      //var name2 = args[2].getClass().getName()
      var _ctx = null;
      if( args[1] instanceof Context){ //name1 == "com.lucidworks.apollo.pipeline.impl.DefaultContext"  ||  name1 == "com.lucidworks.apollo.pipeline.Context"){
         _ctx = args[1]; //Index Pipeline
         rtnObj = args[0]
      }else if( args[2] instanceof Context){//name2 == "com.lucidworks.apollo.pipeline.impl.DefaultContext" || name2 == "com.lucidworks.apollo.pipeline.Context"){
        _ctx = args[2]; // Query Pipeline
      }else{
         throw "Unable to find a " + Context + " in the arguments list, can not register library " + libName;
      }
      //register libName in context object
      if (! _ctx.containsKey(libName)){
          _ctx.put(libName,lib);

        //logger.info("Registering {} in context as {}",libName,lib)
      }else{
        logger.warn("Context already has a {} set {}",libName,_ctx)
      }
      if(rtnObj){
        return args[0];
      }
    }
})();
