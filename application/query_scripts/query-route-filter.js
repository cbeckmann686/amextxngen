function (request, response, ctx, collection, solrServer, solrServerFactory) {
    with (DSL) {
        
        var SOLR_TXN_COLLECTION = "amex_txn_meta";
        
        SolrLib = ctx.get('SolrJHelper');
        
        if (request.getFirstParam("q") === null) {
            logger.info("Query is null! Aborting...");
            return;
        }

        if (request.getFirstParam("fq") === null) {
            logger.info("Filter query is null! Aborting...")
            return;
        } else if (request.getFirstParam("fq").split(':')[0] !== "account_token") {
            logger.error("This query pipeline requires that the filter query specify a value for account_token.")
            return;
        }

        var custId = request.getFirstParam("fq").split(':')[1];

        if (custId === undefined) {
            logger.info("Customer ID (account_token) is undefined. Aborting...")
            return;
        }

        //logger.info("Customer ID: " + custId);
        if (custId.equals("test")) return;

        var SolrQuery = Java.type('org.apache.solr.client.solrj.SolrQuery');
        var SolrServer = solrServerFactory.getSolrServer(SOLR_TXN_COLLECTION);
        var queryString = "id:" + custId;
        var query = new SolrQuery(queryString);
        //logger.info("Query for meta: " + query);
        //query.setFields("collection_name");

        try {
            var resp = SolrLib.querySolr(query, SolrServer);
            if (resp) {
                var result = resp.getResults();
                var targetCollection = (result.get("docs")).get("collection_name");
                var numRecords = (result.get("docs")).get("tx_count");
                ctx.collection = targetCollection;
            }
        } catch (e) {
            logger.error("Error during query:" + e);
            return null;
        }
    }
}
