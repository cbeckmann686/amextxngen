function (request, response, ctx, collection, solrServer, solrServerFactory) {
    with (DSL) {
        var HOUR_COLLECTION_PREFIX = "hour";
        var NUM_HOUR_COLLECTIONS = 4;
        var SOLR_TXN_COLLECTION = "amex_txn_meta";

        SolrLib = ctx.get('SolrJHelper');
        var today = new Date();
        var hour = today.getHours();
        var collectionHour = (hour + 1) % NUM_HOUR_COLLECTIONS;

        var collectionName = HOUR_COLLECTION_PREFIX + collectionHour;
        logger.debug("Attempting to clear collection " + collectionName);
        
        // Clear the hourly collection
        var SolrQuery = Java.type('org.apache.solr.client.solrj.SolrQuery');
        var SolrServer = solrServerFactory.getSolrServer(collectionName);
        var queryString = "*:*";

        try {
            var resp = SolrLib.deleteByQuery(queryString, SolrServer, 30000);

            if (resp) {
                logger.info("Successfully cleared collection " + collectionName + ": " + resp);
            }
        } catch (e) {
            logger.error("Error during hours collection clearing: " + e);
        }

        // Now remove relevant entries from the meta collection
        SolrServer = solrServerFactory.getSolrServer(SOLR_TXN_COLLECTION);
        queryString = "collection_name:" + collectionName;
        logger.info("meta query: " + queryString);

        try {
            var resp = SolrLib.deleteByQuery(queryString, SolrServer, 30000);
            if (resp) {
                logger.info("Successfully purged " + collectionName + " records from meta collection.");
            } else {
                logger.error("Error encountered while purging " + collectionName + " records from meta collection!");
            }
        } catch (e) {
            logger.error("Error during meta collection clearing: " +e);
        }

        return null;
    }
}